<?php

/**
 * @module			openestate
 * @author			Andreas Rudolph, Walter Wagner, cms-lab
 * @copyright		2018-2019 Andreas Rudolph, Walter Wagner, cms-lab
 * @license			please see info.php of this module 
 * @license_terms	please see info.php of this module 
 * @platform		see info.php of this module
 *
 */

// include class.secure.php to protect this file and the whole CMS!
if (defined('LEPTON_PATH')) {   
   include(LEPTON_PATH.'/framework/class.secure.php');
} else {
   $oneback = "../";
   $root = $oneback;
   $level = 1;
   while (($level < 10) && (!file_exists($root.'/framework/class.secure.php'))) {
      $root .= $oneback;
      $level += 1;
   }
   if (file_exists($root.'/framework/class.secure.php')) {
      include($root.'/framework/class.secure.php');
   } else {
      trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
   }
}
// end include class.secure.php

use \OpenEstate\PhpExport\Utils;
use const \OpenEstate\PhpExport\VERSION;
//use function \htmlspecialchars as html;

//get instance of own module class
$oOE = openestate::getInstance();
$oOE->init_section( $page_id, $section_id );

// load current settings from the database
$settings = null;

if($oOE->oe_settings['settings'] == 'new')
{
	echo(LEPTON_tools::display($oOE->language['error_no_settings'],'pre','ui message'));
    return 0;	

} else {	

	$settings = unserialize($oOE->decoded_settings);	
	if (!is_array($settings)) {
		$settings = array();
	}
}


// append ID of current section to the settings
$settings['section_id'] = $section_id;

// init script environment
$scriptPath = isset($settings['env_path']) ? $settings['env_path'] : '';
if (strlen($scriptPath) > 0 && substr($scriptPath, -1) != '/') {
    $scriptPath .= '/';
}
$scriptUrl = isset($settings['env_url']) ? $settings['env_url'] : '';
if (strlen($scriptUrl) > 0 && substr($scriptUrl, -1) != '/') {
    $scriptUrl .= '/';
}
$errors = array();
$environment = $oOE->openestate_wrapper_env($scriptPath,$scriptUrl,true, $settings,$errors);

// make sure, that the script environment was properly loaded
if ($environment === null || count($errors) > 0) {
    echo '<h3>' .$oOE->language['error_invalid_settings']. '</h3>';
    return;
}

// make sure, that the script environment is not currently updated
if (is_file(OpenEstate\PhpExport\Utils::joinPath($environment->getConfig()->basePath, 'immotool.php.lock'))) {
    echo '<h3>' .$oOE->language['error_update_running']. '</h3>';
    echo '<p>' .$oOE->language['error_update_running_info']. '</p>';
    return;
}

// determine the script to load
$wrap = (isset($_REQUEST['wrap'])) ? $_REQUEST['wrap'] : null;
if (!is_string($wrap) && isset($settings['env_script'])) {
    $wrap = $settings['env_script'];
}

// setup language
if (isset($settings['language'])) {
    $lang = (isset($settings['language'])) ? strtolower(trim($settings['language'])) : null;
    if ($lang != null && $environment->isSupportedLanguage($lang)) {
        $environment->setLanguage($lang);
    }
}

ob_start();

try {
    // process the requested action, if necessary
    $actionResult = $environment->processAction();

    // wrap expose view
    if (strtolower($wrap) == 'expose') {

        $view = $environment->newExposeHtml();

        $exposeSettings = (isset($settings['expose']) && is_array($settings['expose'])) ?
            $settings['expose'] : array();

        if ($view->getObjectId() == null) {
            $view->setObjectId(isset($exposeSettings['id']) ? $exposeSettings['id'] : null);
        }

    } // wrap favorite view
    else if (strtolower($wrap) == 'fav') {

        $view = $environment->newFavoriteHtml();

        $favSettings = (isset($settings['fav']) && is_array($settings['fav'])) ?
            $settings['fav'] : array();

        if (!isset($_REQUEST['wrap']) && !isset($_REQUEST['update'])) {
            $environment->getSession()->setFavoritePage(null);
            $environment->getSession()->setFavoriteView(
                (isset($favSettings['view'])) ? $favSettings['view'] : null);
            $environment->getSession()->setFavoriteOrder(
                (isset($favSettings['order_by'])) ? $favSettings['order_by'] : null);
            $environment->getSession()->setFavoriteOrderDirection(
                (isset($favSettings['order_dir'])) ? $favSettings['order_dir'] : null);
        }

    } // wrap listing view by default
    else {

        $view = $environment->newListingHtml();

        $indexSettings = (isset($settings['index']) && is_array($settings['index'])) ?
            $settings['index'] : array();

        if (!isset($_REQUEST['wrap']) && !isset($_REQUEST['update'])) {
            $environment->getSession()->setListingPage(null);
            $environment->getSession()->setListingView(
                (isset($indexSettings['view'])) ? $indexSettings['view'] : null);
            $environment->getSession()->setListingFilters(
                (isset($indexSettings['filter'])) ? $indexSettings['filter'] : null);
            $environment->getSession()->setListingOrder(
                (isset($indexSettings['order_by'])) ? $indexSettings['order_by'] : null);
            $environment->getSession()->setListingOrderDirection(
                (isset($indexSettings['order_dir'])) ? $indexSettings['order_dir'] : null);
        }
    }

    // generate content
    echo $view->process();

    // writer header elements before content
    foreach ($view->getHeaders() as $header) {
        if ($header instanceof \OpenEstate\PhpExport\Html\Javascript) {
            echo "\n<!--(MOVE) JS HEAD BTM- -->\n" . $header->generate() . "\n<!--(END)-->";
        } else if ($header instanceof \OpenEstate\PhpExport\Html\Stylesheet) {
            echo "\n<!--(MOVE) CSS HEAD BTM- -->\n" . $header->generate() . "\n<!--(END)-->";
        } else if ($header instanceof \OpenEstate\PhpExport\Html\Meta) {
            if ($header->name == 'description') {
                echo "\n<!--(REPLACE) META DESC -->\n" . $header->generate() . "\n<!--(END)-->";
            } else if ($header->name == 'keywords') {
                echo "\n<!--(REPLACE) META KEY -->\n" . $header->generate() . "\n<!--(END)-->";
            }
        }
    }

    // write custom css
    $customCss = (isset($settings['css'])) ? trim($settings['css']) : '';
    if ($customCss !== '') {
        echo "\n" . '<!--(MOVE) CSS HEAD BTM- -->';
        echo "\n" . '<style type="text/css">';
        echo "\n" . $customCss;
        echo "\n" . '</style>';
        echo "\n" . '<!--(END)-->';
    }

    // change page title for expose views
    if ($view instanceof \OpenEstate\PhpExport\View\ExposeHtml) {
        echo "\n<!--(REPLACE) TITLE -->\n<title>" . $view->getTitle() . "</title>\n<!--(END)-->";
    }

} catch (\Exception $e) {

    //Utils::logError($e);
    Utils::logWarning($e);

    // ignore previously buffered output
    ob_end_clean();

    echo '<h3>' .$oOE->language['error_internal']. '</h3>'
        . '<p>' . $e->getMessage() . '</p>'
        . '<pre>' . $e . '</pre>';

} finally {

    $content = ob_get_clean();
    $environment->shutdown();
    echo $content;

}
