<?php

/**
 * @module			openestate
 * @author			Andreas Rudolph, Walter Wagner, cms-lab
 * @copyright		2018-2019 Andreas Rudolph, Walter Wagner, cms-lab
 * @license			please see info.php of this module 
 * @license_terms	please see info.php of this module 
 * @platform		see info.php of this module
 *
 */
 
 use \OpenEstate\PhpExport\Utils;

// include class.secure.php to protect this file and the whole CMS!
if (defined('LEPTON_PATH')) {   
   include(LEPTON_PATH.'/framework/class.secure.php');
} else {
   $oneback = "../";
   $root = $oneback;
   $level = 1;
   while (($level < 10) && (!file_exists($root.'/framework/class.secure.php'))) {
      $root .= $oneback;
      $level += 1;
   }
   if (file_exists($root.'/framework/class.secure.php')) {
      include($root.'/framework/class.secure.php');
   } else {
      trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
   }
}
// end include class.secure.php


// Get section ID of the requested wrapper page
if (!isset($_REQUEST['section']) || !is_numeric($_REQUEST['section'])) {
    echo 'Invalid section was specified!';
    return;
} else {
	$section_id = $_REQUEST['section'];
}

//get instance of own module class
$oOE = openestate::getInstance();
$oOE->init_section( NULL, $section_id );

// load current settings
$settings = unserialize($oOE->decoded_settings);
if (!($settings)) {    
	echo '<h3>' .$oOE->language['error_no_settings']. '</h3>';
    return;
}
if (!is_array($settings)) {
	$settings = array();
}

// append ID of current section to the settings
$settings['section_id'] = $section_id;

// init script environment
$scriptPath = isset($settings['env_path']) ? $settings['env_path'] : '';
if (strlen($scriptPath) > 0 && substr($scriptPath, -1) != '/') {
    $scriptPath .= '/';
}
$scriptUrl = isset($settings['env_url']) ? $settings['env_url'] : '';
if (strlen($scriptUrl) > 0 && substr($scriptUrl, -1) != '/') {
    $scriptUrl .= '/';
}
$errors = array();
$environment = $oOE->openestate_wrapper_env(
    $scriptPath,
    $scriptUrl,
    true,
    $settings,
    $errors
);

// make sure, that the script environment was properly loaded
if ($environment === null || count($errors) > 0) {
    echo '<h3>' .$oOE->language['error_invalid_settings']. '</h3>';
    return;
}

try {
    // process the requested action, if necessary
    $actionResult = $environment->processAction();

    // send the result of the requested action
    ob_start();
    if ($actionResult === null) {
        \http_response_code(501);
        echo Utils::getJson(array('error' => 'No action was executed!'));
    } else {
        echo Utils::getJson($actionResult);
    }

} catch (\Exception $e) {

    //Utils::logError($e);
    Utils::logWarning($e);

    // ignore previously buffered output
    \ob_end_clean();
    \ob_start();

    if (!\headers_sent()) {
        \http_response_code(500);
    }
    echo Utils::getJson(array('error' => $e->getMessage()));
    exit(0);

} finally {

    $actionResult = ob_get_clean();
    $environment->shutdown();
    echo $actionResult;
    exit(0);

}