<?php

/**
 * @module			openestate
 * @author			Andreas Rudolph, Walter Wagner, cms-lab
 * @copyright		2018-2019 Andreas Rudolph, Walter Wagner, cms-lab
 * @license			please see info.php of this module 
 * @license_terms	please see info.php of this module 
 * @platform		see info.php of this module
 *
 */

$files_to_register = array(
	'modify.php',
	'wrapper-action.php',
	'save.php'
);

LEPTON_secure::getInstance()->accessFiles( $files_to_register );

?>