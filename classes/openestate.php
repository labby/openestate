<?php

/**
 * @module			openestate
 * @author			Andreas Rudolph, Walter Wagner, cms-lab
 * @copyright		2018-2019 Andreas Rudolph, Walter Wagner, cms-lab
 * @license			please see info.php of this module 
 * @license_terms	please see info.php of this module 
 * @platform		see info.php of this module
 *
 */

use \OpenEstate\PhpExport\Utils;
use const \OpenEstate\PhpExport\VERSION;

class openestate extends LEPTON_abstract
{
	public $database = 0;
	public $page_link = 0;
	public $all_errors = array();
	public $oe_settings = array();
	public $secure_settings = array('settings');
	public $decoded_settings = '';
	public $addon_color = 'olive';
	public $addon_path = LEPTON_PATH.'/modules/openestate/';
	public $action_url = LEPTON_URL.'/modules/openestate/';	
	public $view_url = LEPTON_URL.PAGES_DIRECTORY;
	public $support_link = "<a href=\"https://cms-lab.com/live-support/livehelp.php?department=2\" onclick=\"window.open(this.href,'','resizable=no,location=no,menubar=no,scrollbars=no,status=no,toolbar=no,fullscreen=no,dependent=no,width=650,height=450,status'); return false\">Live-Support / FAQ</a>";	
	public $readme_link = "<a href=\"https://cms-lab.com/_documentation/openestate/readme.php\" target=\"_blank\">Readme</a>";	
	
	public static $instance;

	public function initialize() 
	{
		$this->database = LEPTON_database::getInstance();
		$this->init_section();
	}
	
	public function init_section( $iPageID = 0, $iSectionID = 0 )
	{
		$this->page_link = $this->database->get_one("SELECT link FROM ".TABLE_PREFIX."pages WHERE page_id=". $iPageID."");
		
		//get settings
		$this->oe_settings = array();
		$this->database->execute_query(
			"SELECT * FROM ".TABLE_PREFIX."mod_openestate WHERE section_id=". $iSectionID."  " ,
			true,
			$this->oe_settings,
			false
		);	
		
		// decode settings field
		$this->decoded_settings = $this->database->secure_get_one("SELECT settings FROM ".TABLE_PREFIX."mod_openestate WHERE section_id=". $iSectionID." ");
		
	}
	
    /**
     * Init script environment.
     *
     * @param string $scriptPath
     * Path, that contains to the script environment.
     *
     * @param string $scriptUrl
     * URL, that points to the script environment.
     *
     * @param boolean $initSession
     * Initialize the user session.
     *
     * @param array $settings
     * Associative array, that holds wrapper settings.
     *
     * @param array $errors
     * Errors during initialization.
     *
     * @return Environment
     * The initialized environment or null, if initialization failed.
     */
    public function openestate_wrapper_env($scriptPath, $scriptUrl, $initSession, $settings, &$errors)
    {
        $module_i18n = $this->language;

        if (!is_dir($scriptPath)) {
            $errors[] = $module_i18n['error_no_export_path'];
            return null;
        }

        if (is_file($scriptPath . 'include/functions.php')) {
            if (!defined('IN_WEBSITE')) {
                define('IN_WEBSITE', 1);
            }
            if (!defined('IMMOTOOL_BASE_PATH')) {
                define('IMMOTOOL_BASE_PATH', $scriptPath);
            }

            /** @noinspection PhpIncludeInspection */
            require_once($scriptPath . 'include/functions.php');

            $oldVersionNumber = (defined('IMMOTOOL_SCRIPT_VERSION')) ? IMMOTOOL_SCRIPT_VERSION : '???';
            $errors[] = $module_i18n['error_old_version'] . ' (' . $oldVersionNumber . ')';
        } else if (!is_file($scriptPath . 'index.php') ||
            !is_file($scriptPath . 'expose.php') ||
            !is_file($scriptPath . 'fav.php') ||
            !is_file($scriptPath . 'config.php') ||
            !is_dir($scriptPath . 'include') ||
            !is_dir($scriptPath . 'include/OpenEstate') ||
            !is_file($scriptPath . 'include/init.php')
        ) {
            $errors[] = $module_i18n['error_invalid_export_path'];
        }
        if (count($errors) > 0) {
			$this->all_errors = $errors;
            return null;
        }

        /** @noinspection PhpIncludeInspection */
        require_once($scriptPath . 'include/init.php');

        /** @noinspection PhpIncludeInspection */
        require_once($scriptPath . 'config.php');

        if (!defined('OpenEstate\PhpExport\VERSION')) {
            $errors[] = $module_i18n['error_unknown_version'];
            return null;
        }

        require_once($this->addon_path.'wrapper-config.php');

        try {
            $config = new OpenEstate\PhpExport\WrapperConfig($scriptPath, $scriptUrl, $settings);
            //echo '<pre>' . print_r( $config, true ) . '</pre>';
            return new OpenEstate\PhpExport\Environment($config, $initSession);
        } catch (\Exception $e) {
            $errors[] = $module_i18n['error_init'] . ' ' . $e->getMessage();;
            return null;
        }
    }	
	

} // end of class
?>