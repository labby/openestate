### Openestate
============

This module integrates real estates from [OpenEstate-ImmoTool][3] into your website.

#### Requirements

* [LEPTON CMS][1], Version >= 4.0


#### Installation

* download latest [.zip][2] installation archive
* in CMS backend select the file from "Add-ons" -> "Modules" -> "Install module"

#### Notice

After installing addon you are done. <br />
Please create a new page in the backend (selecting this addon) and use it!


[1]: https://lepton-cms.org "LEPTON CMS"
[2]: http://www.lepton-cms.com/lepador/modules/openestate.php
[3]: https://openestate.org

