<?php

/**
 * @module			openestate
 * @author			Andreas Rudolph, Walter Wagner, cms-lab
 * @copyright		2018-2019 Andreas Rudolph, Walter Wagner, cms-lab
 * @license			please see info.php of this module 
 * @license_terms	please see info.php of this module 
 * @platform		see info.php of this module
 *
 */

// include class.secure.php to protect this file and the whole CMS!
if (defined('LEPTON_PATH')) {   
   include(LEPTON_PATH.'/framework/class.secure.php');
} else {
   $oneback = "../";
   $root = $oneback;
   $level = 1;
   while (($level < 10) && (!file_exists($root.'/framework/class.secure.php'))) {
      $root .= $oneback;
      $level += 1;
   }
   if (file_exists($root.'/framework/class.secure.php')) {
      include($root.'/framework/class.secure.php');
   } else {
      trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
   }
}
// end include class.secure.php

//	create table
$table_fields="
	`section_id` INT NOT NULL DEFAULT -1,
	`page_id` INT NOT NULL DEFAULT -1,
	`settings` TEXT,
	 PRIMARY KEY ( `section_id` )
	";
LEPTON_handle::install_table("mod_openestate", $table_fields);

// check for errors
if ($database->is_error()) {
	$admin->print_error($database->get_error());
}

// Insert info into the search table
// Module query info
$field_info = array();
$field_info['page_id'] = 'page_id';
$field_info['title'] = 'page_title';
$field_info['link'] = 'link';
$field_info['description'] = 'description';
$field_info['modified_when'] = 'modified_when';
$field_info['modified_by'] = 'modified_by';
$field_info = serialize($field_info);

// Query start
$query_start_code = "SELECT [TP]pages.page_id, [TP]pages.page_title,	[TP]pages.link, [TP]pages.description, [TP]pages.modified_when, [TP]pages.modified_by	FROM [TP]mod_openestate, [TP]pages WHERE ";

// Query body
$query_body_code =  " [TP]pages.page_id = [TP]mod_openestate.page_id AND [TP]mod_openestate.simple_output [O] \'[W][STRING][W]\' AND [TP]pages.searching = \'1\'";
	
$field_values="
	(NULL,'module', 'openestate', '".$field_info."'),	
	(NULL,'query_start', '".$query_start_code."', 'openestate'),
	(NULL,'query_body', '".$query_body_code."', 'openestate'),
	(NULL,'query_end', '', 'openestate')
";
LEPTON_handle::insert_values('search', $field_values);

// create directory for exported/uploaded files
LEPTON_handle::register( "make_dir" );
make_dir(LEPTON_PATH.'/modules/openestate/export_data'); 	
?>	
