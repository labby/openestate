<?php

/**
 * @module			openestate
 * @author			Andreas Rudolph, Walter Wagner, cms-lab
 * @copyright		2018-2019 Andreas Rudolph, Walter Wagner, cms-lab
 * @license			please see info.php of this module 
 * @license_terms	please see info.php of this module 
 * @platform		see info.php of this module
 *
 */

// include class.secure.php to protect this file and the whole CMS!
if (defined('LEPTON_PATH')) {   
   include(LEPTON_PATH.'/framework/class.secure.php');
} else {
   $oneback = "../";
   $root = $oneback;
   $level = 1;
   while (($level < 10) && (!file_exists($root.'/framework/class.secure.php'))) {
      $root .= $oneback;
      $level += 1;
   }
   if (file_exists($root.'/framework/class.secure.php')) {
      include($root.'/framework/class.secure.php');
   } else {
      trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
   }
}
// end include class.secure.php


$oOE = openestate::getInstance();
$oOE->init_section( $_POST['page_id'], $_POST['section_id'] );

$js_back = 'javascript: history.go(-1);';
$admin = LEPTON_admin::getInstance();

if(isset($_POST['job']) && $_POST['job'] == 'first_call' ) {
	$settings = array();
} else {
	$settings = unserialize($this->decoded_settings);
}

$default_language = trim($admin->get_post('language'));
$language = (!file_exists($oOE->addon_path.'languages/'.strtolower($default_language).'.php')) ? 'en' : $default_language;
// put changes into configuration of the current page
//load_default_immotool_settings($settings);
$settings['env_path'] = trim($admin->get_post('env_path'));
$settings['env_url'] = trim($admin->get_post('env_url'));
$settings['env_script'] = trim($admin->get_post('env_script'));
$settings['language'] = $language;
$settings['charset'] = 'UTF-8';
$settings['css'] = '';
$settings['listingUrl'] = trim($admin->get_post('listingUrl'));
$settings['favUrl'] = trim($admin->get_post('favUrl'));
$settings['exposeUrl'] = trim($admin->get_post('exposeUrl'));

// save additional settings for the selected view
unset($settings['index']);
unset($settings['expose']);
unset($settings['fav']);
if ($settings['env_script'] === 'index') {
    $settings['index'] = $admin->get_post('index');
} else if ($settings['env_script'] === 'expose') {
    $settings['expose'] = $admin->get_post('expose');
} else if ($settings['env_script'] === 'fav') {
    $settings['fav'] = $admin->get_post('fav');
}

// save disabled components
$allComponents = explode(',', trim($admin->get_post('allComponents')));
$components = $admin->get_post('component');
if (!is_array($components)) $components = array();
$settings['disabledComponents'] = array();
foreach ($allComponents as $componentId) {
    if (!in_array($componentId, $components))
        $settings['disabledComponents'][] = $componentId;
}

// save enabled features components
$features = $admin->get_post('feature');
$settings['features'] = (is_array($features)) ?
    $features : array();

// make sure, that the path ends with a slash
$len = strlen($settings['env_path']);
if ($len > 0 && $settings['env_path'][$len - 1] != '/') {
    $settings['env_path'] .= '/';
}

// make sure, that the URL ends with a slash
$len = strlen($settings['env_url']);
if ($len > 0 && $settings['env_url'][$len - 1] != '/') {
    $settings['env_url'] .= '/';
}

// save modified settings	
$code_settings = array('settings'=> serialize($settings));
$result = $oOE->database->secure_build_and_execute( 'UPDATE', TABLE_PREFIX."mod_openestate", $code_settings ,"section_id = ".$section_id, $oOE->secure_settings);

if($result == false) {
	echo (LEPTON_tools::display($oOE->database->get_error(),'pre','ui green message'));
}

if ($database->is_error()) {
    $admin->print_error($database->get_error(), $js_back);
} else {
    $admin->print_success($MESSAGE['PAGES_SAVED'], ADMIN_URL . '/pages/modify.php?page_id=' . $page_id);
}

// print admin footer
$admin->print_footer();
