/**
 * @module			openestate
 * @author			Andreas Rudolph, Walter Wagner, cms-lab
 * @copyright		2018-2019 Andreas Rudolph, Walter Wagner, cms-lab
 * @license			please see info.php of this module 
 * @license_terms	please see info.php of this module 
 * @platform		see info.php of this module
 *
 */

function show_wrapper_settings($value) {
	document.getElementById('openestate_index_settings').style.visibility = ($value === 'index') ? 'visible' : 'collapse';
    document.getElementById('openestate_expose_settings').style.visibility = ($value === 'expose') ? 'visible' : 'collapse';
    document.getElementById('openestate_fav_settings').style.visibility = ($value === 'fav') ? 'visible' : 'collapse';
}


