<?php

/**
 * @module			openestate
 * @author			Andreas Rudolph, Walter Wagner, cms-lab
 * @copyright		2018-2019 Andreas Rudolph, Walter Wagner, cms-lab
 * @license			please see info.php of this module 
 * @license_terms	please see info.php of this module 
 * @platform		see info.php of this module
 *
 */
 
// include class.secure.php to protect this file and the whole CMS!
if (defined('LEPTON_PATH')) {	
	include(LEPTON_PATH.'/framework/class.secure.php'); 
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.'/framework/class.secure.php'))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.'/framework/class.secure.php')) { 
		include($root.'/framework/class.secure.php'); 
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php

//Modul Description
$module_description = 'Dieses Modul integriert das OpenEstate-ImmoTool in deine Webseite.';

//Variables for the backend
$MOD_OPENESTATE = array(

	// Allgemein
	'setup'  => 'Anbindung der exportierten PHP-Skripte',
	'view'  => 'Darstellung auf der Seite',

	// Anbindung
	'setup_validate'  => 'Überprüfung',
	'setup_success'  => 'Die ImmoTool-Skripte sind korrekt eingebunden!',
	'setup_problem'  => 'Die ImmoTool-Skripte sind NICHT korrekt eingebunden!',
	'setup_errors'  => 'Fehlermeldungen',
	'setup_step_export'  => 'Führen Sie einen PHP-Export via ImmoTool auf diesen Webspace durch.',
	'setup_step_config'  => 'Tragen Sie den Pfad und URL des Exportes ein und klicken Sie zur erneuten Prüfung auf \'Speichern\'.',
	'setup_path'  => 'Skript PFAD',
	'setup_path_info'  => 'Tragen Sie hier den Pfad des Servers ein, wo die vom ImmoTool erzeugten Skripte abgelegt wurden. Der Standard Pfad lautet:',
	'setup_url'  => 'Skript URL',
	'setup_url_info'  => 'Tragen Sie hier die Webadresse ein, über welche der ImmoTool-Export aus dem Internet erreichbar ist. Die Standard Url lautet:',

	// Immobilienübersicht
	'view_index'  => 'Immobilienübersicht',
	'view_index_view'  => 'Ansicht',
	'view_index_view_detail'  => 'als Liste',
	'view_index_view_thumb'  => 'als Galerie',
	'view_index_order'  => 'Sortierung',
	'view_index_order_asc'  => 'aufsteigend',
	'view_index_order_desc'  => 'absteigend',
	'view_index_filter'  => 'nach %s filtern',

	// Exposéansicht
	'view_expose'  => 'Exposéansicht',
	'view_expose_id'  => 'ID der Immobilie',

	// Vormerkliste
	'view_fav'  => 'Vormerkliste',
	'view_fav_view'  => 'Ansicht',
	'view_fav_view_detail'  => 'als Liste',
	'view_fav_view_thumb'  => 'als Galerie',
	'view_fav_order'  => 'Sortierung',
	'view_fav_order_asc'  => 'aufsteigend',
	'view_fav_order_desc'  => 'absteigend',

	// Optionen
	'options'  => 'Weitere Optionen',
	'options_language'  => 'Sprache',
	'options_language_info'  => 'Diese Sprache wird für die eingebundenen Inhalte verwendet.',
	'options_charset'  => 'Zeichensatz',
	'options_charset_info'  => 'Tragen Sie den Zeichensatz ein, der auf der Webseite verwendet wird.',
	'options_css'  => 'Stylesheet',
	'options_css_info'  => 'Bei Bedarf können Stylesheets hinterlegt werden, die bei der Einbindung zusätzlich geladen werden.',
	'options_components'  => 'Komponenten',
	'options_components_info'  => 'Der PHP-Export integriert folgende Komponenten von Drittanbietern in die Webseite. Wenn einzelne Komponenten bereits auf Ihrer Webseite genutzt werden, können diese hier deaktiviert werden.',
	'options_features'  => 'Funktionen',
	'options_features_filtering'  => 'Filterung der Immobilienübersicht aktivieren.',
	'options_features_ordering'  => 'Sortierung von Immobilienlisten aktivieren.',
	'options_features_favorites'  => 'Vormerkliste aktivieren.',
	'options_features_languages'  => 'Auswahl der Sprache aktivieren.',
	'options_listingUrl'  => 'Angebots-URL',
	'options_listingUrl_info'  => 'Bei Bedarf kann eine URL eingetragen werden um von dieser Seite auf Angebotslisten zu verlinken. Wenn keine URL angegeben wurde, werden Angebotslisten auf der aktuellen Seite dargestellt.',
	'options_favUrl'  => 'Favoriten-URL',
	'options_favUrl_info'  => 'Bei Bedarf kann eine URL eingetragen werden um von dieser Seite auf die Vormerkliste zu verlinken. Wenn keine URL angegeben wurde, wird die Vormerkliste auf der aktuellen Seite dargestellt.',
	'options_exposeUrl'  => 'Exposé-URL',
	'options_exposeUrl_info'  => 'Bei Bedarf kann eine URL eingetragen werden um von dieser Seite auf Einzelansichten einer Immobilie zu verlinken. Wenn keine URL angegeben wurde, werden Einzelansichten auf der aktuellen Seite dargestellt.',

	// Fehler
	'error_no_settings'  => 'Keine Einstellungen zu dieser Seite gefunden!',
	'error_invalid_settings'  => 'Die Einstellungen zu dieser Seite sind ungültig!',
	'error_update_running'  => 'Der Immobilienbestand wird momentan aktualisiert!',
	'error_update_running_info'  => 'Bitte besuchen Sie diese Seite in wenigen Minuten erneut.',
	'error_internal'  => 'Ein interner Fehler ist aufgetreten!',
	'error_no_export_path'  => 'Bitte tragen Sie einen gültigen Export-Pfad ein!',
	'error_invalid_export_path'  => 'Es ist anscheinend kein PHP-Export unter dem angegebenen Pfad vorhanden!',
	'error_old_version'  => 'Sie verwenden anscheinend eine nicht unterstützte Version des PHP-Exports.',
	'error_unknown_version'  => 'Die Version des PHP-Exports kann nicht ermittelt werden!',
	'error_init'  => 'Der PHP-Export kann nicht initialisiert werden!'
);

?>
