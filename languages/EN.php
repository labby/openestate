<?php

/**
 * @module			openestate
 * @author			Andreas Rudolph, Walter Wagner, cms-lab
 * @copyright		2018-2019 Andreas Rudolph, Walter Wagner, cms-lab
 * @license			please see info.php of this module 
 * @license_terms	please see info.php of this module 
 * @platform		see info.php of this module
 *
 */
 
// include class.secure.php to protect this file and the whole CMS!
if (defined('LEPTON_PATH')) {	
	include(LEPTON_PATH.'/framework/class.secure.php'); 
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.'/framework/class.secure.php'))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.'/framework/class.secure.php')) { 
		include($root.'/framework/class.secure.php'); 
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php

//Modul Description
$module_description = 'This module integrates real estates from OpenEstate-ImmoTool into your website.';

//Variables for the backend
$MOD_OPENESTATE = array(

	// Allgemein
	'setup'  =>  'Configure exported scripts',
	'view'  =>  'Configure generated view',

	// Anbindung
	'setup_validate'  =>  'Validation',
	'setup_success'  =>  'The exported scripts are correctly configured!',
	'setup_problem'  =>  'The exported scripts are NOT correctly configured!',
	'setup_errors'  =>  'error messages',
	'setup_step_export'  =>  'Export your properties from ImmoTool to your website via PHP.',
	'setup_step_config'  =>  'Configure PATH and URL, that points to the exported scripts, and click \'Save\' to perform a new validation.',
	'setup_path'  =>  'script PATH',
	'setup_path_info'  =>  'Enter the path on your server, that points to the exported scripts. The default PATH is:',
	'setup_url'  =>  'script URL',
	'setup_url_info'  =>  'Enter the URL on your server, that points to the exported scripts. The default URL is:',

	// Immobilienübersicht
	'view_index'  =>  'Property listing',
	'view_index_view'  =>  'view',
	'view_index_view_detail'  =>  'as table',
	'view_index_view_thumb'  =>  'as thumbnails',
	'view_index_order'  =>  'order',
	'view_index_order_asc'  =>  'ascending',
	'view_index_order_desc'  =>  'descending',
	'view_index_filter'  =>  'filter by %s',

	// Exposéansicht
	'view_expose'  =>  'Property details',
	'view_expose_id'  =>  'property ID',

	// Vormerkliste
	'view_fav'  =>  'Favorites',
	'view_fav_view'  =>  'view',
	'view_fav_view_detail'  =>  'as table',
	'view_fav_view_thumb'  =>  'as thumbnails',
	'view_fav_order'  =>  'order',
	'view_fav_order_asc'  =>  'ascending',
	'view_fav_order_desc'  =>  'descending',

	// Optionen
	'options'  =>  'Further options',
	'options_language'  =>  'language',
	'options_language_info'  =>  'This language is used for the embedded contents.',
	'options_charset'  =>  'charset',
	'options_charset_info'  =>  'Enter the charset, that is used on this website.',
	'options_css'  =>  'stylesheet',
	'options_css_info'  =>  'You can provide custom stylesheets, that are loaded together with the PHP export.',
	'options_components'  =>  'components',
	'options_components_info'  =>  'The PHP export integrates these third party components into your website. If your website already uses some of these components, you can disable them accordingly.',
	'options_features'  =>  'features',
	'options_features_filtering'  =>  'Enable filtering of object listings.',
	'options_features_ordering'  =>  'Enable ordering of object listings.',
	'options_features_favorites'  =>  'Enable favorites.',
	'options_features_languages'  =>  'Enable language selection.',
	'options_listingUrl'  =>  'listings URL',
	'options_listingUrl_info'  =>  'You may enter an URL, that is used to link object listings from this page. If no URL is provided, listings are shown on the current page.',
	'options_favUrl'  =>  'favorites URL',
	'options_favUrl_info'  =>  'You may enter an URL, that is used to link favorite listings from this page. If no URL is provided, favorites are shown on the current page.',
	'options_exposeUrl'  =>  'object URL',
	'options_exposeUrl_info'  =>  'You may enter an URL, that is used to link single objects from this page. If no URL is provided, single objects are shown on the current page.',

	// Fehler
	'error_no_settings'  =>  'Can\t find settings for this page!',
	'error_invalid_settings'  =>  'The settings for this page are invalid!',
	'error_update_running'  =>  'The properties are currently updated!',
	'error_update_running_info'  =>  'Please revisit this page after some minutes.',
	'error_internal'  =>  'An internal error occurred!',
	'error_no_export_path'  =>  'Please enter a valid script path!',
	'error_invalid_export_path'  =>  'It seems, that there is no PHP export available within the script path.',
	'error_old_version'  =>  'It seems, that you\'re using an unsupported version of PHP export.',
	'error_unknown_version'  =>  'Can\'t detect the script version!',
	'error_init'  =>  'Can\'t init script environment!'
);

?>

