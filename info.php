<?php

/**
 * @module			openestate
 * @author			Andreas Rudolph, Walter Wagner, cms-lab
 * @copyright		2018-2019 Andreas Rudolph, Walter Wagner, cms-lab
 * @license			please see info.php of this module 
 * @license_terms	please see info.php of this module 
 * @platform		see info.php of this module
 *
 */

// include class.secure.php to protect this file and the whole CMS!
if (defined('LEPTON_PATH')) {	
	include(LEPTON_PATH.'/framework/class.secure.php'); 
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.'/framework/class.secure.php'))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.'/framework/class.secure.php')) { 
		include($root.'/framework/class.secure.php'); 
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php
 

$module_directory	= 'openestate';
$module_name		= 'OpenEstate Wrapper';
$module_function	= 'page';
$module_version		= '1.0.0';
$module_platform	= '4.x';
$module_delete		=  true;
$module_author		= 'Andreas Rudolph, Walter Wagner, CMS-LAB';
$module_license     = "<a href='https://cms-lab.com/_documentation/openestate/license.php' target='-blank'>GNU General Public License version 3</a>";
$module_license_terms = "<a href='https://cms-lab.com/_documentation/openestate/license.php' target='-blank'>License Terms</a>";
$module_description	= 'This module integrates real estates from OpenEstate-ImmoTool into your website.';
$module_guid		= '69cd82fd-ff08-425e-806c-c8747ac4ec52';

?>
