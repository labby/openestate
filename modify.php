<?php

/**
 * @module			openestate
 * @author			Andreas Rudolph, Walter Wagner, cms-lab
 * @copyright		2018-2019 Andreas Rudolph, Walter Wagner, cms-lab
 * @license			please see info.php of this module 
 * @license_terms	please see info.php of this module 
 * @platform		see info.php of this module
 *
 */
 
// include class.secure.php to protect this file and the whole CMS!
if (defined('LEPTON_PATH')) {   
   include(LEPTON_PATH.'/framework/class.secure.php');
} else {
   $oneback = "../";
   $root = $oneback;
   $level = 1;
   while (($level < 10) && (!file_exists($root.'/framework/class.secure.php'))) {
      $root .= $oneback;
      $level += 1;
   }
   if (file_exists($root.'/framework/class.secure.php')) {
      include($root.'/framework/class.secure.php');
   } else {
      trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
   }
}
// end include class.secure.php


//get instance of own module class
$oOE = openestate::getInstance();
$oOE->init_section( $page_id, $section_id );

if(isset ($_POST['job']) && ($_POST['job']== 'show_info') ) {
	
	$support_link = "<a href=\"#\">NO Live-Support / FAQ</a>";
	
	// template marker
	$form_values = array(
		'oOE'			=> $oOE,
		'section_id'	=> $section_id,
		'page_id'		=> $page_id,
		'SUPPORT'		=> $support_link,
		'image_url'		=> 'https://cms-lab.com/_documentation/media/openestate/openestate.png',	
		'leptoken'		=> get_leptoken()
	);

	/**	
	 *	get the template-engine.
	 */
	$oTwig = lib_twig_box::getInstance();
	$oTwig->registerModule('openestate');
		
	echo $oTwig->render( 
		"@openestate/info.lte",	//	template-filename
		$form_values				//	template-data
	);	
} else {



	// get page infos from the database
	$pageLink = $oOE->page_link;

	// check if page is called for the first time
	if($oOE->oe_settings['settings'] != 'new')
	{	
		// load current settings from the database
		$settings = unserialize($oOE->decoded_settings);
echo(LEPTON_tools::display($oOE->decoded_settings,'pre','ui message'));
echo(LEPTON_tools::display($settings,'pre','ui message'));			
		if (!is_array($settings)) {
			$settings = array();
		}
		// append ID of current section to the settings
		$settings['section_id'] = $section_id;

		// init script environment
		$errors = array();
		$envPath = (isset($settings['env_path'])) ? $settings['env_path'] : '';
		$envUrl = (isset($settings['env_url'])) ? $settings['env_url'] : '';
		$envScript = (isset($settings['env_script'])) ? $settings['env_script'] : null;
		if ($envScript == null || $envScript == '') $envScript = 'index';
		$environment = $oOE->openestate_wrapper_env($envPath, $envUrl, false, $settings, $errors);
		if(count($oOE->all_errors) > 0 ) {
			$display_errors = true;
		} else {
			$display_errors = false;
		}
		// set current language, if available
		if ($environment !== null) {
			$valid = true;
//echo(LEPTON_tools::display($oOE->all_errors,'pre','ui message'));			
			$lang = defined('DEFAULT_LANGUAGE') ? strtolower(DEFAULT_LANGUAGE) : 'en';
			if ($environment->isSupportedLanguage($lang)) {
				$environment->setLanguage($lang);
			} else {
				Utils::createTranslator($environment)->register();
			}		
				
            // load index settings
            $indexSettings = (isset($settings['index']) && is_array($settings['index'])) ? $settings['index'] : array();
			
            if (!isset($indexSettings['view']))
                $indexSettings['view'] = 'detail';
            if (!isset($indexSettings['order_by']))
                $indexSettings['order_by'] = 'ObjectId';
            if (!isset($indexSettings['order_dir']))
                $indexSettings['order_dir'] = 'desc';

            // load expose settings
            $exposeSettings = (isset($settings['expose']) && is_array($settings['expose'])) ? $settings['expose'] : array();
            if (!isset($exposeSettings['id']))
                $exposeSettings['id'] = '';

            // load favorite settings
            $favSettings = (isset($settings['fav']) && is_array($settings['fav'])) ? $settings['fav'] : array();
            if (!isset($favSettings['view']))
                $favSettings['view'] = 'detail';
            if (!isset($favSettings['order_by']))
                $favSettings['order_by'] = 'ObjectId';
            if (!isset($favSettings['order_dir']))
                $favSettings['order_dir'] = 'desc';

            // load further further_settings
            if (!isset($further_settings['language']) || $further_settings['language'] == '')
                $further_settings['language'] = $environment->getLanguage();
            if (!isset($further_settings['charset']) || $further_settings['charset'] == '')
                $further_settings['charset'] = $environment->getConfig()->charset;
            if (!isset($further_settings['css']))
                $further_settings['css'] = '';
            if (!isset($further_settings['disabledComponents']) || !is_array($further_settings['disabledComponents']))
                $further_settings['disabledComponents'] = array();
            if (!isset($further_settings['features']) || !is_array($further_settings['features']))
                $further_settings['features'] = array();
            if (!isset($further_settings['listingUrl']))
                $further_settings['listingUrl'] = '';
            if (!isset($further_settings['favUrl']))
                $further_settings['favUrl'] = '';
            if (!isset($further_settings['exposeUrl']))
                $further_settings['exposeUrl'] = '';


			// build index options
			$orders = array();
			$titles_options = array();
			foreach ($environment->getConfig()->getOrderObjects() as $orderObj) {
				$name = $orderObj->getName();
				$titles_options[$name] = strtolower($orderObj->getTitle($environment->getLanguage()));
				$orders[$name] = $orderObj;
			}
			asort($titles_options);

			// build index filter	
			$filters = array();
			$titles_filters = array();
			foreach ($environment->getConfig()->getFilterObjects() as $filterObj) {
				$name = $filterObj->getName();
				$filters[$name] = $filterObj;
				$titles_filters[$name] = strtolower($filterObj->getTitle($environment->getLanguage()));
			}
			asort($titles_filters);		

			// build filter tags
			$all_filters = array();
			foreach (array_keys($titles_filters) as $name) {
				$filterObj = $filters[$name];
				$filterValue = (isset($settings) && isset($settings['immotool_index']['filter'][$name])) ? $settings['immotool_index']['filter'][$name] : '';

				// create filter widget
				$filterWidget = $filterObj->getWidget($environment, $filterValue);
				$filterWidget->id = 'openestate_index_filter_' . $name;
				//$filterWidget->name = 'index[filter][' . $name . ']';
				$filterWidget->name = sprintf($oOE->language['view_index_filter'],'<q>'.$filterObj->getTitle($environment->getLanguage()).'</q>');	
				$generated_widgets = $filterWidget->generate();
				$all_filters[] = array('filter'=>$filterWidget,'generated_widgets'=>$generated_widgets);
			}

// echo LEPTON_tools::display($settings);

			// build fav order
			$orders = array();
			$titles_fav = array();
			foreach ($environment->getConfig()->getOrderObjects() as $orderObj) {
				$name = $orderObj->getName();
				$titles_fav[$name] = strtolower($orderObj->getTitle($environment->getLanguage()));
				$orders[$name] = $orderObj;
			}
			asort($titles_fav);	
			
			$form_values = array(
				'oOE'			=> $oOE,
				'section_id'	=> $section_id,
				'page_id'		=> $page_id,
				'export_version'=> \OpenEstate\PhpExport\VERSION,
				'display_errors'=> $display_errors,
				'settings'		=> $settings,
				'indexSettings'	=>$indexSettings,
				'exposeSettings'=>$exposeSettings,
				'favSettings'	=>$favSettings,
				'further_settings'	=>$further_settings,
				'titles_options'	=>$titles_options,
				'titles_fav'	=>$titles_fav,
				'all_filters'	=>$all_filters,
				'orders'		=>$orders,
				'allComponents_string' => implode(',', $environment->getTheme()->getComponentIds()),
				'allComponents'	=> $environment->getTheme()->getComponentIds(),
				'new'			=> false,
				'valid'			=> $valid,
				'readme'		=> 'https://cms-lab.com/_documentation/openestate/readme.php',			
				'leptoken'		=> get_leptoken()
			);				
		
		} else {
				// environment == NULL
				$display_errors = true;				
				$valid = false;
				$form_values = array(
					'oOE'			=> $oOE,
					'section_id'	=> $section_id,
					'page_id'		=> $page_id,
					'display_errors'=> $display_errors,
					'new'			=> true,
					'valid'			=> $valid,
					'readme'		=> 'https://cms-lab.com/_documentation/openestate/readme.php',			
					'leptoken'		=> get_leptoken()
				);
				
		}
	
	} else 
	{
		$display_errors = false;
		$valid = false;
		$form_values = array(
			'oOE'			=> $oOE,
			'section_id'	=> $section_id,
			'page_id'		=> $page_id,
			'display_errors'=> $display_errors,
			'new'			=> true,
			'valid'			=> $valid,
			'readme'		=> 'https://cms-lab.com/_documentation/openestate/readme.php',			
			'leptoken'		=> get_leptoken()
		);
	
	}

	//	get the template-engine.
	$oTwig = lib_twig_box::getInstance();
	$oTwig->registerModule('openestate');
		
	echo $oTwig->render( 
		"@openestate/modify.lte",	//	template-filename
		$form_values				//	template-data
	);		
	
}





